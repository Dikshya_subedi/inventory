﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace InventorySystem.Migrations
{
    public partial class salesbillingrelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Sales_BillNo",
                table: "Sales",
                column: "BillNo");

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_Billings_BillNo",
                table: "Sales",
                column: "BillNo",
                principalTable: "Billings",
                principalColumn: "BillId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sales_Billings_BillNo",
                table: "Sales");

            migrationBuilder.DropIndex(
                name: "IX_Sales_BillNo",
                table: "Sales");
        }
    }
}
