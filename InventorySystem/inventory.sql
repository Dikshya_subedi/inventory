USE [InventorySystemDb]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 11/14/2021 12:59:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Billings]    Script Date: 11/14/2021 12:59:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Billings](
	[BillId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[DiscountRate] [nvarchar](max) NULL,
	[Subtotal] [float] NOT NULL,
	[Discount] [float] NOT NULL,
	[Total] [float] NOT NULL,
	[BillDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Billings] PRIMARY KEY CLUSTERED 
(
	[BillId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 11/14/2021 12:59:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[PhoneNo] [nvarchar](max) NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Items]    Script Date: 11/14/2021 12:59:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Items](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](100) NOT NULL,
	[Category] [int] NOT NULL,
	[Unit] [nvarchar](max) NULL,
	[Quantity] [int] NOT NULL,
	[PurchasePrice] [int] NOT NULL,
	[SalePrice] [int] NOT NULL,
	[Date] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Items] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 11/14/2021 12:59:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](100) NOT NULL,
	[PurchasePrice] [int] NOT NULL,
	[SalePrice] [int] NOT NULL,
	[PurchaseDate] [datetime2](7) NOT NULL,
	[Category] [int] NOT NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductTypes]    Script Date: 11/14/2021 12:59:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductTypeName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_ProductTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Purchases]    Script Date: 11/14/2021 12:59:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Purchases](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](100) NOT NULL,
	[Category] [int] NOT NULL,
	[Unit] [nvarchar](max) NULL,
	[Quantity] [int] NOT NULL,
	[PurchasePrice] [int] NOT NULL,
	[SalePrice] [int] NOT NULL,
	[TotalAmount] [real] NOT NULL,
	[PurchaseDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Purchases] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sales]    Script Date: 11/14/2021 12:59:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sales](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](100) NOT NULL,
	[Category] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[SalePrice] [int] NOT NULL,
	[TotalAmount] [int] NOT NULL,
	[SalesDate] [datetime2](7) NOT NULL,
	[Unit] [nvarchar](max) NULL,
	[BillNo] [int] NOT NULL,
 CONSTRAINT [PK_Sales] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 11/14/2021 12:59:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](20) NULL,
	[Email] [nvarchar](max) NOT NULL,
	[Contact] [int] NOT NULL,
	[Password] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210520095522_Create DataBase', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210520100749_add DateTime', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210524101708_table for sales', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210524153404_ProductType table added', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210527083608_PropertyName_change_ProductTypeName', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210527160315_sales_property_Type_ChangeDataType_int', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210527161400_sales column renamed', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210527161712_sales column renamed1', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210527163051_dalli budi1', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210527163641_dalli budi meri', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210528085509_add column Category', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210601125828_User table added', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20211020161031_purchase table', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20211020162747_datatype changed-unit-int to string', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20211021095220_item table created', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20211022035726_unit added to sales table', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20211023050842_string length updated for productname', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20211113100814_BillNo added to sales', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20211114023546_Billing added', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20211114025454_sales billing relation', N'5.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20211114053905_billing date', N'5.0.6')
SET IDENTITY_INSERT [dbo].[Billings] ON 

INSERT [dbo].[Billings] ([BillId], [CustomerId], [DiscountRate], [Subtotal], [Discount], [Total], [BillDate]) VALUES (1, 1, N'5%', 1000, 50, 950, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Billings] ([BillId], [CustomerId], [DiscountRate], [Subtotal], [Discount], [Total], [BillDate]) VALUES (6, 1, NULL, 0, 0, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Billings] ([BillId], [CustomerId], [DiscountRate], [Subtotal], [Discount], [Total], [BillDate]) VALUES (7, 1, NULL, 0, 0, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Billings] ([BillId], [CustomerId], [DiscountRate], [Subtotal], [Discount], [Total], [BillDate]) VALUES (8, 1, NULL, 0, 0, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Billings] ([BillId], [CustomerId], [DiscountRate], [Subtotal], [Discount], [Total], [BillDate]) VALUES (9, 1, NULL, 0, 0, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Billings] ([BillId], [CustomerId], [DiscountRate], [Subtotal], [Discount], [Total], [BillDate]) VALUES (10, 1, NULL, 0, 0, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Billings] ([BillId], [CustomerId], [DiscountRate], [Subtotal], [Discount], [Total], [BillDate]) VALUES (11, 1, NULL, 0, 0, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Billings] ([BillId], [CustomerId], [DiscountRate], [Subtotal], [Discount], [Total], [BillDate]) VALUES (12, 1, NULL, 0, 0, 0, CAST(N'2021-11-14T11:34:39.0413202' AS DateTime2))
INSERT [dbo].[Billings] ([BillId], [CustomerId], [DiscountRate], [Subtotal], [Discount], [Total], [BillDate]) VALUES (13, 1, NULL, 0, 0, 0, CAST(N'2021-11-14T12:40:45.2821401' AS DateTime2))
SET IDENTITY_INSERT [dbo].[Billings] OFF
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([Id], [Name], [PhoneNo]) VALUES (1, N'Suman', N'9806759332')
INSERT [dbo].[Customers] ([Id], [Name], [PhoneNo]) VALUES (9, N'Ram Bahadur', N'9806759123')
SET IDENTITY_INSERT [dbo].[Customers] OFF
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([Id], [ProductName], [PurchasePrice], [SalePrice], [PurchaseDate], [Category]) VALUES (35, N'dhara oil 1lt', 200, 230, CAST(N'2021-10-23T10:02:00.0000000' AS DateTime2), 7072)
INSERT [dbo].[Products] ([Id], [ProductName], [PurchasePrice], [SalePrice], [PurchaseDate], [Category]) VALUES (36, N'dhara oil 2lt', 190, 450, CAST(N'2021-10-23T10:03:00.0000000' AS DateTime2), 7072)
INSERT [dbo].[Products] ([Id], [ProductName], [PurchasePrice], [SalePrice], [PurchaseDate], [Category]) VALUES (37, N'dhara oil pack', 2200, 2500, CAST(N'2021-10-22T10:04:00.0000000' AS DateTime2), 7072)
INSERT [dbo].[Products] ([Id], [ProductName], [PurchasePrice], [SalePrice], [PurchaseDate], [Category]) VALUES (38, N'Mung dal', 175, 186, CAST(N'2021-10-22T10:05:00.0000000' AS DateTime2), 7070)
INSERT [dbo].[Products] ([Id], [ProductName], [PurchasePrice], [SalePrice], [PurchaseDate], [Category]) VALUES (39, N'Mass dal', 153, 158, CAST(N'2021-10-22T10:06:00.0000000' AS DateTime2), 7070)
INSERT [dbo].[Products] ([Id], [ProductName], [PurchasePrice], [SalePrice], [PurchaseDate], [Category]) VALUES (40, N'Rahara dal', 175, 179, CAST(N'2021-10-22T10:07:00.0000000' AS DateTime2), 7070)
INSERT [dbo].[Products] ([Id], [ProductName], [PurchasePrice], [SalePrice], [PurchaseDate], [Category]) VALUES (41, N'Musuro dal', 155, 160, CAST(N'2021-10-22T10:07:00.0000000' AS DateTime2), 7070)
INSERT [dbo].[Products] ([Id], [ProductName], [PurchasePrice], [SalePrice], [PurchaseDate], [Category]) VALUES (42, N'Chana dal', 161, 165, CAST(N'2021-10-21T10:08:00.0000000' AS DateTime2), 7070)
INSERT [dbo].[Products] ([Id], [ProductName], [PurchasePrice], [SalePrice], [PurchaseDate], [Category]) VALUES (43, N'jeera masino rice 25kg', 1700, 1800, CAST(N'2021-10-22T10:09:00.0000000' AS DateTime2), 7071)
INSERT [dbo].[Products] ([Id], [ProductName], [PurchasePrice], [SalePrice], [PurchaseDate], [Category]) VALUES (44, N'jeera masino rice 1kg', 72, 80, CAST(N'2021-10-22T10:10:00.0000000' AS DateTime2), 7071)
INSERT [dbo].[Products] ([Id], [ProductName], [PurchasePrice], [SalePrice], [PurchaseDate], [Category]) VALUES (45, N'premier jeeramasino rice 25kg', 1825, 1950, CAST(N'2021-10-22T10:12:00.0000000' AS DateTime2), 7071)
INSERT [dbo].[Products] ([Id], [ProductName], [PurchasePrice], [SalePrice], [PurchaseDate], [Category]) VALUES (46, N'Hulas jeeramasino rice 25kg', 2050, 2130, CAST(N'2021-10-22T10:13:00.0000000' AS DateTime2), 7071)
INSERT [dbo].[Products] ([Id], [ProductName], [PurchasePrice], [SalePrice], [PurchaseDate], [Category]) VALUES (47, N'test', 123, 130, CAST(N'2021-10-12T22:15:00.0000000' AS DateTime2), 7069)
SET IDENTITY_INSERT [dbo].[Products] OFF
SET IDENTITY_INSERT [dbo].[ProductTypes] ON 

INSERT [dbo].[ProductTypes] ([Id], [ProductTypeName]) VALUES (6, N'Stationary')
INSERT [dbo].[ProductTypes] ([Id], [ProductTypeName]) VALUES (7068, N'Dairy')
INSERT [dbo].[ProductTypes] ([Id], [ProductTypeName]) VALUES (7069, N'Battery')
INSERT [dbo].[ProductTypes] ([Id], [ProductTypeName]) VALUES (7070, N'Pulse')
INSERT [dbo].[ProductTypes] ([Id], [ProductTypeName]) VALUES (7071, N'Rice')
INSERT [dbo].[ProductTypes] ([Id], [ProductTypeName]) VALUES (7072, N'Oil')
INSERT [dbo].[ProductTypes] ([Id], [ProductTypeName]) VALUES (7074, N'Chips')
INSERT [dbo].[ProductTypes] ([Id], [ProductTypeName]) VALUES (7075, N'Water')
INSERT [dbo].[ProductTypes] ([Id], [ProductTypeName]) VALUES (7077, N'Beverage')
INSERT [dbo].[ProductTypes] ([Id], [ProductTypeName]) VALUES (7085, N'Cold Drinks')
INSERT [dbo].[ProductTypes] ([Id], [ProductTypeName]) VALUES (7086, N'Grocery')
SET IDENTITY_INSERT [dbo].[ProductTypes] OFF
SET IDENTITY_INSERT [dbo].[Sales] ON 

INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (2, N'jeera masino rice 25kg', 7071, 3, 1800, 5400, CAST(N'2021-10-12T10:55:00.0000000' AS DateTime2), NULL, 1)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1002, N'jeera masino rice 25kg', 7071, 1, 1800, 1800, CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2), NULL, 1)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1003, N'dhara oil 2lt', 7072, 3, 450, 1350, CAST(N'2021-10-27T18:16:00.0000000' AS DateTime2), NULL, 1)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1004, N'dhara oil 2lt', 7072, 1, 450, 450, CAST(N'2021-10-27T18:22:00.0000000' AS DateTime2), NULL, 1)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1005, N'jeera masino rice 25kg', 7071, 1, 1800, 1800, CAST(N'2021-10-27T18:28:00.0000000' AS DateTime2), NULL, 1)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1006, N'premier jeeramasino rice 25kg', 7071, 1, 1950, 1950, CAST(N'2021-10-27T18:30:00.0000000' AS DateTime2), NULL, 1)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1007, N'Hulas jeeramasino rice 25kg', 7071, 1, 2130, 2130, CAST(N'2021-10-27T18:39:00.0000000' AS DateTime2), NULL, 1)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1008, N'dhara oil 1lt', 7072, 4, 230, 920, CAST(N'2021-10-31T09:28:00.0000000' AS DateTime2), NULL, 1)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1009, N'jeera masino rice 1kg', 7071, 3, 80, 240, CAST(N'2021-10-31T14:38:00.0000000' AS DateTime2), NULL, 1)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1011, N'Hulas jeeramasino rice 25kg', 7071, 1, 2130, 2130, CAST(N'2021-11-14T10:25:00.0000000' AS DateTime2), NULL, 6)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1012, N'Hulas jeeramasino rice 25kg', 7071, 1, 2130, 2130, CAST(N'2021-11-14T10:31:00.0000000' AS DateTime2), NULL, 7)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1013, N'Hulas jeeramasino rice 25kg', 7071, 1, 2130, 2130, CAST(N'2021-11-14T10:33:00.0000000' AS DateTime2), NULL, 8)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1014, N'dhara oil 1lt', 7072, 1, 230, 230, CAST(N'2021-11-14T10:36:00.0000000' AS DateTime2), NULL, 9)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1015, N'Hulas jeeramasino rice 25kg', 7071, 1, 2130, 2130, CAST(N'2021-11-14T10:43:00.0000000' AS DateTime2), NULL, 10)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1016, N'Hulas jeeramasino rice 25kg', 7071, 1, 2130, 2130, CAST(N'2021-11-14T10:50:00.0000000' AS DateTime2), NULL, 11)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1017, N'dhara oil 1lt', 7072, 1, 230, 230, CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2), NULL, 11)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1018, N'Hulas jeeramasino rice 25kg', 7071, 1, 2130, 2130, CAST(N'2021-11-14T11:33:00.0000000' AS DateTime2), NULL, 12)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1019, N'dhara oil pack', 7072, 1, 2500, 2500, CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2), NULL, 12)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1020, N'Mung dal', 7070, 1, 186, 186, CAST(N'2021-11-14T12:39:00.0000000' AS DateTime2), NULL, 13)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1021, N'Rahara dal', 7070, 2, 179, 358, CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2), NULL, 13)
INSERT [dbo].[Sales] ([Id], [ProductName], [Category], [Quantity], [SalePrice], [TotalAmount], [SalesDate], [Unit], [BillNo]) VALUES (1022, N'Musuro dal', 7070, 3, 160, 480, CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2), NULL, 13)
SET IDENTITY_INSERT [dbo].[Sales] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([Id], [UserName], [Email], [Contact], [Password]) VALUES (1, N'dd', N'dd', 11, N'dd')
INSERT [dbo].[Users] ([Id], [UserName], [Email], [Contact], [Password]) VALUES (2, N'qq', N'dd', 11, N'ff')
INSERT [dbo].[Users] ([Id], [UserName], [Email], [Contact], [Password]) VALUES (3, N'suman', N'33', 44, N'aa')
INSERT [dbo].[Users] ([Id], [UserName], [Email], [Contact], [Password]) VALUES (4, N'priya', N'aa@gmail.com', 111, N'111')
INSERT [dbo].[Users] ([Id], [UserName], [Email], [Contact], [Password]) VALUES (5, N'stuti', N'22', 11, N'33')
INSERT [dbo].[Users] ([Id], [UserName], [Email], [Contact], [Password]) VALUES (6, N'ss', N'11', 22, N'ss')
INSERT [dbo].[Users] ([Id], [UserName], [Email], [Contact], [Password]) VALUES (7, N'qq', N'22', 11, N'33')
SET IDENTITY_INSERT [dbo].[Users] OFF
ALTER TABLE [dbo].[Billings] ADD  DEFAULT ('0001-01-01T00:00:00.0000000') FOR [BillDate]
GO
ALTER TABLE [dbo].[Products] ADD  DEFAULT ('0001-01-01T00:00:00.0000000') FOR [PurchaseDate]
GO
ALTER TABLE [dbo].[Products] ADD  DEFAULT ((0)) FOR [Category]
GO
ALTER TABLE [dbo].[Sales] ADD  DEFAULT ((0)) FOR [BillNo]
GO
ALTER TABLE [dbo].[Billings]  WITH CHECK ADD  CONSTRAINT [FK_Billings_Customers_CustomerId] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Billings] CHECK CONSTRAINT [FK_Billings_Customers_CustomerId]
GO
ALTER TABLE [dbo].[Sales]  WITH CHECK ADD  CONSTRAINT [FK_Sales_Billings_BillNo] FOREIGN KEY([BillNo])
REFERENCES [dbo].[Billings] ([BillId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sales] CHECK CONSTRAINT [FK_Sales_Billings_BillNo]
GO
