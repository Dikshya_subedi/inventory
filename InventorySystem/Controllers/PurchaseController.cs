﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using InventorySystem.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

namespace InventorySystem.Controllers
{
    [Authorize]
    public class PurchaseController : Controller
    {
        private readonly AppdbContext _context;
        public PurchaseController(AppdbContext context)
        {
            _context = context;
        }
        //Get:Product/Index
        public IActionResult Index() => View();

        public JsonResult GetAll() => Json(_context.Purchases.ToList());

        [HttpGet]
        public IActionResult GetForm(int? id)
        {
            var purchase = _context.Purchases.Find(id);
            var category = _context.ProductTypes.Select(a => new SelectListItem { Value = a.Id.ToString(), Text = a.ProductTypeName }).ToList();
            ViewBag.Category = category;
            return PartialView("_form", purchase);
        }

        [HttpGet]
        public JsonResult Delete(int id)
        {
            var product = _context.Purchases.Find(id);
            _context.Remove(product);
            _context.SaveChanges();
            return Json(product);

        }

        [HttpPost]
        public JsonResult SaveForm(Purchase product)
        {
            if (ModelState.IsValid)
            {
                if (product.Id == 0)
                    _context.Add(product);
                else
                    _context.Update(product);

                _context.SaveChanges();
            }
            return Json(product);
        }

        public JsonResult GetProduct(string term)
        {
            return Json(_context.Products.Where(x => x.ProductName.Contains(term)).
                        Select(x => new
                        {
                            Product = x.ProductName,
                            category = x.Category,
                            Quantity = 1
                        }).ToList());

            //return _context.Products.Where(x => x.ProductName.Contains(term)).Select(x => x.ProductName).ToArray();
        }

    }
}