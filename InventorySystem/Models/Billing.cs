﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystem.Models
{
    public class Billing
    {
        [Key]
        public int BillId { get; set; }

        public int CustomerId { get; set; }

        public string DiscountRate { get; set; }

        public double Subtotal { get; set; }

        public double  Discount { get; set; }

        public double Total { get; set; }

        public DateTime BillDate { get; set; }
        public Customer Customer { get; set; }
        public List<Sales> Sales { get; set; }
    }
}
