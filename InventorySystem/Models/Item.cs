﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace InventorySystem.Models
{
    public class Item
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string ProductName { get; set; }
  
        public int Category { get; set; }

        public string Unit { get; set; }

        public int Quantity { get; set; }

        public int PurchasePrice { get; set; }

        public int SalePrice { get; set; }
        public DateTime Date { get; set; }
    }
}
