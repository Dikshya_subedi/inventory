﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;
using InventorySystem.Models;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Authorization;

namespace InventorySystem.Controllers
{
    [Authorize(Roles = "Admin,Manager")]
    public class PredictionController : Controller
    {

        private readonly AppdbContext _context;

        public PredictionController(AppdbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        public JsonResult GetAll() => Json(_context.MonthlySales.Select(s => new { date = s.Date.ToString("yyyy-MM-dd"), sales = s.Sales }).ToList());


        public async Task<JsonResult> TestAsync(string _data)
        {

            var data = new StringContent(_data, Encoding.UTF8, "application/json");
            var parameters = new Dictionary<string, string> { { "param1", _data } };
            var encodedContent = new FormUrlEncodedContent(parameters);

            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMinutes(5);
                client.BaseAddress = new Uri("http://127.0.0.1:5000/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method
                HttpResponseMessage response = await client.PostAsync("/start", encodedContent);
                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<PredModel>(content);
                    return Json(result);
                }
                return null;

            }
        }
    }

}