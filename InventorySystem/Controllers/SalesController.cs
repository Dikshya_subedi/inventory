﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using InventorySystem.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using Microsoft.AspNetCore.Authorization;

namespace InventorySystem.Controllers
{
    [Authorize]
    public class SalesController : Controller
    {
        private readonly AppdbContext _context;
        public SalesController(AppdbContext context)
        {
            _context = context;
        }
        //Get:Sales/Index
        public IActionResult Index() => View();

        public JsonResult GetAll()
        {
            var sales = _context.Sales.Select(s => new
            {
                Id = s.Id,
                ProductName = s.ProductName,
                Quantity = s.Quantity,
                SalePrice = s.SalePrice,
                SalesDate = s.SalesDate,
                TotalAmount = s.TotalAmount,
                ProductType = s.ProductType,

            }).ToList();

            return Json(sales);
        }

        [HttpGet]
        public IActionResult GetForm(int? id)
        {
            var sales = _context.Sales.Find(id);
            if (sales != null)
            {
                var custId = _context.Billings.Where(b => b.BillId == sales.BillNo).First().CustomerId;
                var customer = _context.Customers.Where(c => c.Id == custId).First().Name;
                sales.Billing.Customer.Name = customer;
            }


            var category = _context.ProductTypes.Select(a => new SelectListItem { Value = a.Id.ToString(), Text = a.ProductTypeName }).ToList();
            var productName = _context.Products.Select(a => new SelectListItem { Value = a.ProductName, Text = a.ProductName }).ToList();
            ViewBag.Category = category;
            ViewBag.ProductName = productName;
            return PartialView("_form", sales);
        }

        [HttpGet]
        public JsonResult Delete(int id)
        {
            var sales = _context.Sales.Find(id);
            _context.Remove(sales);
            _context.SaveChanges();
            return Json(sales);

        }

        public int SaveCustomer(Sales sales)
        {

            Customer customer = new Customer();
            customer.Name = sales.Billing.Customer.Name;
            customer.PhoneNo = sales.Billing.Customer.PhoneNo;
            customer.Date = sales.SalesDate;
            _context.Add(customer);
            _context.SaveChanges();
            return customer.Id;
        }

        public int SaveBill(Sales sales, int custId)
        {
            Billing billing = new Billing();
            billing.CustomerId = custId;
            billing.BillDate = sales.SalesDate;
            billing.Customer = null;
            _context.Add(billing);
            _context.SaveChanges();
            return billing.BillId;
        }

        [HttpPost]
        public JsonResult SaveForm(Sales sales, int rows)
        {
            if (ModelState.IsValid)
            {
                sales.Profit = (sales.SalePrice - sales.PurchasePrice) * sales.Quantity;
                if (sales.Id == 0)
                {
                    //save and get the billNo and customer first 
                    int billNo = 0;
                    int custId = 0;
                    if (rows == 0)
                    {
                        custId = SaveCustomer(sales);
                        billNo = SaveBill(sales, custId);
                    }
                    else
                        billNo = _context.Billings.OrderByDescending(b => b.BillId).FirstOrDefault().BillId;

                    sales.BillNo = billNo;
                    sales.Billing = null;
                    _context.Add(sales);
                }
                else
                    _context.Update(sales);
                _context.SaveChanges();
            }
            return Json(sales);
        }

        public JsonResult GetProduct(string term)
        {
            return Json(_context.Products.Where(x => x.ProductName.Contains(term)).
                Select(x => new
                {
                    Product = x.ProductName,
                    PurchasePrice = x.PurchasePrice,
                    SalePrice = x.SalePrice,
                    category = x.Category,
                    Quantity = 1
                }).ToList());
        }

    }
}


