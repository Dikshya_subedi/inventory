﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using InventorySystem.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using InventorySystem.Common;

namespace InventorySystem.Controllers
{
    [Authorize]
    public class SalesReportController : Controller
    {
        private readonly AppdbContext _context;
        QueryHelper _queryHelper = new QueryHelper();
        public SalesReportController(AppdbContext context)
        {
            _context = context;
            //_queryHelper = queryHelper;
        }

        //GET:Sales/SalesReport
        public IActionResult Index()
        {
            var productTypes = _context.ProductTypes.Select(a => new SelectListItem { Value = a.Id.ToString(), Text = a.ProductTypeName }).ToList();
            var productName = _context.Products.Select(a => new SelectListItem { Value = a.ProductName, Text = a.ProductName }).ToList();
            ViewBag.Category = productTypes;
            ViewBag.ProductName = productName;

            return View();
        }
        public JsonResult GetSalesReport(string ProductName, int Category, DateTime StartDate, DateTime EndDate)
        {
            EndDate = EndDate.AddHours(23.99);
            var qry = $@"select t.SalesDate as SalesDate, sum(t.TotalAmount) as TotalAmount from(
                        select CAST(SalesDate as date) as SalesDate, TotalAmount from Sales
                            where ProductName = '{ProductName}' and SalesDate >= '{StartDate}' and SalesDate <= '{EndDate}')t
                                group by t.SalesDate";

            var data = _queryHelper.ExecSql<Sales>(qry, _context);
            return Json(data);

        }

        public JsonResult GetSalesQuantityReport(string ProductName, int Category, DateTime StartDate, DateTime EndDate)
        {
            var data = _context.Sales.Where(p => p.ProductName == ProductName && (p.SalesDate >= StartDate && p.SalesDate <= EndDate)).Select(x => new { totalAmount = x.TotalAmount, salesDate = x.SalesDate.ToString("yyyy/MM/dd") }).ToList();
            return Json(data);

        }

        public int GetCategory(string ProductName)
        {
            return _context.Products.Where(p => p.ProductName == ProductName).Select(x => x.Category).SingleOrDefault();
        }





    }
}