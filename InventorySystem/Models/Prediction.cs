using System.Collections.Generic;

namespace InventorySystem.Models
{
    public class PredModel
    {
        public string stationary { get; set; }
        public string seasonalStationary { get; set; }
        public List<PredData> all_data { get; set; }
    }

    public class PredData
    {
        public string date { get; set; }
        public string sales { get; set; }
        public string pred { get; set; }
        public string per_diff { get; set; }
    }

}