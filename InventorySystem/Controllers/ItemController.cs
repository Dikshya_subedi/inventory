﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using InventorySystem.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

namespace InventorySystem.Controllers
{
    [Authorize]
    public class ItemController : Controller
    {
        private readonly AppdbContext _context;
        public ItemController(AppdbContext context)
        {
            _context = context;
        }
        //Get:Product/Index
        public IActionResult Index() => View();

        public JsonResult GetAll() => Json(_context.Items.ToList());

        [HttpGet]
        public IActionResult GetForm(int? id)
        {
            var item = _context.Items.Find(id);
            var category = _context.ProductTypes.Select(a => new SelectListItem { Value = a.Id.ToString(), Text = a.ProductTypeName }).ToList();
            //var category = _context.ProductTypes.ToList();
            ViewBag.Category = category;
            return PartialView("_form", item);
        }

        [HttpGet]
        public JsonResult Delete(int id)
        {
            var item = _context.Items.Find(id);
            _context.Remove(item);
            _context.SaveChanges();
            return Json(item);

        }

        [HttpPost]
        public void SaveForm(Item item)
        {
            if (ModelState.IsValid)
            {
                if (item.Id == 0)
                    _context.Add(item);
                else
                    _context.Update(item);

                _context.SaveChanges();
            }
            // return GetAll();
        }
        public string[] GetProduct(string term)
        {
            return _context.Products.Where(x => x.ProductName.Contains(term)).Select(x => x.ProductName).ToArray();
        }

    }
}