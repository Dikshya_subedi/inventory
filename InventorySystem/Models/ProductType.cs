﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace InventorySystem.Models
{
    public class ProductType
    {
        public int Id { get; set;}

        [Required]
        [StringLength(100)]
        [Display(Name ="Product Type")]
        public string ProductTypeName { get; set; }

        public List<Sales> Sales { get; set; }
        public List<Product> Product { get; set; }
    }
}
