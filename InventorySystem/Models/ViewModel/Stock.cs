﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystem.Models.ViewModel
{
    public class Stock
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public int Purchase { get; set; }
        public int Sales { get; set; }
        public int InStock { get; set; }
    }
}
