﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventorySystem.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InventorySystem.Controllers
{
    [Authorize]
    public class BillingController : Controller
    {

        private readonly AppdbContext _context;
        public BillingController(AppdbContext context)
        {
            _context = context;
        }
        public IActionResult Index() => View();


        public IActionResult Detail(int id)
        {
            try
            {
                var totalCost = _context.Sales.Where(s => s.BillNo == id).GroupBy(s => s.BillNo)
                    .Select(g => g.Sum(i => i.TotalAmount)).FirstOrDefault();


                string customerName = _context.Billings.Where(b => b.BillId == id).Select(s => s.Customer.Name).FirstOrDefault();
                var sales = _context.Sales.Where(s => s.BillNo == id).ToList();

                ViewData["bill"] = id;
                ViewData["date"] = sales[0].SalesDate;
                ViewData["name"] = customerName;
                ViewData["total"] = totalCost;

                return View(sales);
            }
            catch (Exception ex)
            {
            }

            return View();
        }

        public JsonResult GetAll()
        {
            return Json(_context.Billings.Select(b => new { billId = b.BillId, customer = b.Customer.Name, billDate = b.BillDate }).ToList());
        }

        public JsonResult SalesData(int id)
        {
            var sales = _context.Sales.Where(s => s.BillNo == 1).ToList();
            return Json(sales);
        }
    }
}