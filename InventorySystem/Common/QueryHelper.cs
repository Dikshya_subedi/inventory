﻿using InventorySystem.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;

namespace InventorySystem.Common
{
    public class QueryHelper
    {
        public List<T> ExecSql<T>(string query, AppdbContext ctx, SqlParameter[] parameters = null, CommandType commandType = CommandType.Text)
        {
            using (var command = ctx.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = query;
                command.CommandType = commandType;

                if (parameters?.Any() == true)
                {
                    foreach (var sqlParameter in parameters)
                    {
                        command.Parameters.Add(sqlParameter);
                    }
                }

                ctx.Database.OpenConnection();

                List<T> list = new List<T>();
                try
                {
                    using (var result = command.ExecuteReader())
                    {
                        T obj = default(T);
                        while (result.Read())
                        {
                            obj = Activator.CreateInstance<T>();
                            foreach (PropertyInfo prop in obj.GetType().GetProperties())
                            {

                                if (!prop.PropertyType.IsEnum &&
                                    HasColumn(result, prop.Name) &&
                                    !object.Equals(result[prop.Name], DBNull.Value))
                                {
                                    prop.SetValue(obj, result[prop.Name], null);
                                }
                            }
                            list.Add(obj);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    ctx.Database.CloseConnection();
                    throw;
                }

                ctx.Database.CloseConnection();
                return list;
            }
        }

        private bool HasColumn(DbDataReader reader, string columnName)
        {
            var dataRowCollection = reader.GetSchemaTable()?.Rows;

            if (dataRowCollection != null)
                foreach (DataRow row in dataRowCollection)
                {
                    if (row["ColumnName"].ToString() == columnName)
                        return true;
                }

            return false;
        }


    }
}
