﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystem.Models
{
    public class Customer
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string PhoneNo { get; set; }
        
        public Billing Billing { get; set; }

        public DateTime Date { get; set; }

    }
}
