﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InventorySystem.Models;
using InventorySystem.Models.ViewModel;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

namespace InventorySystem.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        private readonly AppdbContext _context;
        public ProductController(AppdbContext context)
        {
            _context = context;
        }
        //Get:Product/Index
        public IActionResult Index() => View();

        public JsonResult GetAll()
        {
            var product = _context.Products.Select(p => new
            {
                Id = p.Id,
                ProductName = p.ProductName,
                ProductType = p.ProductType,
                PurchasePrice = p.PurchasePrice,
                SalePrice = p.SalePrice,
                PurchaseDate = p.PurchaseDate
            });

            return Json(product);
        }

        [HttpGet]
        public IActionResult GetForm(int? id)
        {
            var product = _context.Products.Find(id);
            var category = _context.ProductTypes.Select(a => new SelectListItem { Value = a.Id.ToString(), Text = a.ProductTypeName }).ToList();
            //var category = _context.ProductTypes.ToList();
            ViewBag.Category = category;
            return PartialView("_form", product);
        }

        [HttpGet]
        public JsonResult Delete(int id)
        {
            var product = _context.Products.Find(id);
            _context.Remove(product);
            _context.SaveChanges();
            return Json(product);

        }

        [HttpPost]
        public ActionResult SaveForm(Product product)
        {
            if (ModelState.IsValid)
            {
                if (product.Id == 0)
                    _context.Add(product);
                else
                    _context.Update(product);

                _context.SaveChanges();
                return Json(product);
            }
            return View();

        }
    }
}