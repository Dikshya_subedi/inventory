﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using InventorySystem.Models;
using InventorySystem.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;

namespace InventorySystem.Controllers
{
    [Authorize]
    public class ProductTypeController : Controller
    {

        private readonly AppdbContext _context;
        public ProductTypeController(AppdbContext context)
        {
            _context = context;
        }

        //Get:ProductType/Index
        public IActionResult Index() => View();

        public JsonResult GetAll() => Json(_context.ProductTypes.ToList());

        [HttpPost]
        public JsonResult SaveForm([Bind("Id,ProductTypeName")] ProductType productType)
        {
            if (ModelState.IsValid)
            {
                if (productType.Id == 0)
                    _context.Add(productType);
                else
                    _context.Update(productType);

                _context.SaveChanges();
            }
            return Json(productType);
        }

        [HttpGet]
        public JsonResult Delete(int id)
        {
            var productType = _context.ProductTypes.Find(id);
            _context.Remove(productType);
            _context.SaveChanges();
            return Json(productType);

        }


        [HttpGet]
        public IActionResult GetForm(int? id)
        {
            var productType = _context.ProductTypes.Find(id);
            return PartialView("_form", productType);
        }


    }
}