﻿
using InventorySystem.Models.ViewModel;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace InventorySystem.Models
{
    public class AppdbContext : IdentityDbContext<User, Role, int>
    {
        public AppdbContext(DbContextOptions<AppdbContext> options) : base(options)
        {

        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Billing>()
                .HasOne<Customer>(c => c.Customer)
                .WithOne(s => s.Billing)
                .HasForeignKey<Billing>(p => p.CustomerId);

            modelBuilder.Entity<Sales>()
               .HasOne<Billing>(b => b.Billing)
               .WithMany(s => s.Sales)
               .HasForeignKey(s => s.BillNo);

            modelBuilder.Entity<Sales>()
              .HasOne<ProductType>(p => p.ProductType)
              .WithMany(s => s.Sales)
              .HasForeignKey(s => s.Category);

            modelBuilder.Entity<Product>()
             .HasOne<ProductType>(p => p.ProductType)
             .WithMany(p => p.Product)
             .HasForeignKey(p => p.Category);

            //modelBuilder.Entity<User>()
            //.HasOne<Role>(r => r.role)
            //.WithMany(u => u.Users)
            ////.HasForeignKey(p => p.UserName);
        }



        public DbSet<Product> Products { get; set; }
        public DbSet<Sales> Sales { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }
        //public DbSet<User> Users { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Billing> Billings { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<MonthlySale> MonthlySales { get; set; }
        public DbSet<Profit> Profits { get; set; }

    }
}
