using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using InventorySystem.Models;
using Microsoft.AspNetCore.Identity;

namespace InventorySystem
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public interface IAsyncStartup
        {
            IServiceProvider ConfigureServices(IServiceCollection services);

            Task ConfigureAsync(IApplicationBuilder app);
        }

        public interface IAsyncStartupFilter
        {
            Func<IApplicationBuilder, Task> Configure(Func<IApplicationBuilder, Task> next);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddControllersWithViews();
            services.AddDbContext<AppdbContext>(Options => Options.UseSqlServer(Configuration.GetConnectionString("DBConnection")));

            services.AddIdentity<User, Role>(option =>
              option.SignIn.RequireConfirmedAccount = false)
            .AddEntityFrameworkStores<AppdbContext>();

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings.
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 1;

                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings.
                options.User.AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = false;
            });

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(5);
                options.LoginPath = "/Identity/Account/Login";
                options.AccessDeniedPath = "/Sales/Index";
                options.SlidingExpiration = true;
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, AppdbContext db, UserManager<User> userManager, RoleManager<Role> roleManager)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            db.Database.EnsureCreated();
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            SeedAdminUser(userManager, roleManager);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }

        private void SeedAdminUser(UserManager<User> userManager, RoleManager<Role> roleManager)
        {


            string roleName = "Admin";
            var roleExist = roleManager.RoleExistsAsync(roleName).Result;
            if (!roleExist)
                roleManager.CreateAsync(new Role() { Name = roleName });


            var _user = userManager.FindByEmailAsync("admin@gmail.com").Result;
            if (_user == null)
            {
                var poweruser = new User
                {
                    UserName = "admin",
                    Email = "admin@gmail.com",
                };
                string adminPassword = "admin@00_";
                var createPowerUser = userManager.CreateAsync(poweruser, adminPassword).Result;
                if (createPowerUser.Succeeded)
                {
                    var x = userManager.AddToRoleAsync(poweruser, roleName).Result;
                }

            }
        }
    }
}
