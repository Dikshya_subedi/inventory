﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace InventorySystem.Models.ViewModel
{
    public class SalesViewModel
    {
        public Sales sale { get; set; }
        public List<Sales> sales { get; set; }
        public List<SelectListItem> productTypes { get; set; }
        public List<SelectListItem> productName { get; set; }


    }
}
