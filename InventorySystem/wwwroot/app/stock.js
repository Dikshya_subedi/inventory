﻿
   UpdateStockNotification()

function UpdateStockNotification() {
        $.get('/Stock/GetAll', function (data) {
            var lowStock = data.filter(product => product.inStock < 25)
            var lowStockNotification = lowStock.map(prd => capitalize(prd.name) + ",Quantity left: " + prd.inStock);
            SetNotification(lowStockNotification)
        });
    }

function SetNotification(data) {

     $(".dropdown-arrow .notify-item").remove();

        $(".noti-title span").text(data.length);

        $.each(data, function (k, v) {
            let _data = v.split(',');
            var notificationItem = '<a href="#" class="dropdown-item notify-item">'
                + '<p class="notify-details">'
                + '<b>' + _data[0] + '</b>'
                + '<span>' + _data[1] + '</span>'
                + '</p>'
                + '</a>'
            $(".dropdown-arrow").append(notificationItem)
        })

        let ViewAll = '<a href="stock/Index" class="dropdown-item notify-item allstock"><span> View All Stocks </span> </a>'
        $(".dropdown-arrow").append(ViewAll)

    }

    function capitalize(s) {
        return s[0].toUpperCase() + s.slice(1);
    }

