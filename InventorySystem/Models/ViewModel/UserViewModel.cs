﻿using System;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace InventorySystem.Models.ViewModel
{
    public class UserViewModel
    {
        public int Id  { get; set; }
        public string UserName  { get; set; }
        public int Role  { get; set; }
        public string PhoneNumber  { get; set; }
        public string Password  { get; set; }
       
    }
}
