﻿using System.Linq;
using InventorySystem.Models;
using InventorySystem.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace InventorySystem.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserManagementController : Controller
    {

        private readonly AppdbContext _context;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;

        public UserManagementController(AppdbContext context, SignInManager<User> signInManager, UserManager<User> userManager)
        {
            _context = context;
            _signInManager = signInManager;
            _userManager = userManager;
        }
        public IActionResult Index()
        {
            return View();
        }

        public JsonResult GetAll()
        {
            //var user = _context.Users.Select(p => new {
            //    Id = p.Id,
            //    Name = p.UserName,
            //    Role = p.UserName,
            //    Phone = p.PhoneNumber,
            //});

            //var u = _userManager.Users.Inc

            var u = _context.Users.
                    Join(_context.UserRoles,
                         u => u.Id,
                         ur => ur.UserId,
                         (u, ur) => new { u, ur })
                    .Join(_context.Roles,
                          ur => ur.ur.RoleId,
                          r => r.Id,
                          (x, r) => new { x, r })
                    .Select(z => new
                    {
                        Id = z.x.u.Id,
                        UserName = z.x.u.UserName,
                        Role = z.r.Name,
                        Phone = z.x.u.PhoneNumber
                    }).ToList();





            return Json(u);
        }

        [HttpGet]
        public IActionResult GetForm(string? id)
        {
            var user = _userManager.FindByIdAsync(id).Result;
            var userRole = 0;
            if (user != null)
            {
                userRole = _context.UserRoles.Where(x => x.UserId == int.Parse(id)).FirstOrDefault().RoleId;
            }
            UserViewModel usr = new UserViewModel();
            var roles = _context.Roles.Select(a => new SelectListItem { Value = a.Id.ToString(), Text = a.Name }).ToList();
            ViewBag.Roles = roles;

            if (id != null)
            {
                usr.UserName = user.UserName;
                usr.Role = userRole;
                usr.PhoneNumber = user.PhoneNumber;
            }

            return PartialView("_form", usr);
        }

        [HttpGet]
        public JsonResult Delete(string id)
        {
            var _user = _userManager.FindByIdAsync(id).Result;
            var currentRole = _userManager.GetRolesAsync(_user).Result[0];
            var remove = _userManager.RemoveFromRoleAsync(_user, currentRole).Result;
            if (remove.Succeeded)
            {
                _context.Remove(_user);
                _context.SaveChanges();
            }
            return Json(_user);
        }

        [HttpPost]
        public JsonResult SaveForm(UserViewModel user)
        {
            var _user = _userManager.FindByIdAsync(user.Id.ToString()).Result;
            string newRole = _context.Roles.Find(user.Role).Name;

            if (_user == null)
            {
                User usr = new User();
                usr.UserName = user.UserName;
                usr.PhoneNumber = user.PhoneNumber;
                usr.PasswordHash = user.Password;

                var createUser = _userManager.CreateAsync(usr, user.Password).Result;
                if (createUser.Succeeded)
                {
                    var x = _userManager.AddToRoleAsync(usr, newRole).Result;
                }
            }
            else
            {
                _user.UserName = user.UserName;
                _user.PhoneNumber = user.PhoneNumber;
                _context.Update(_user);
                _context.SaveChanges();

                //update role
                var currentRole = _userManager.GetRolesAsync(_user).Result[0];
                var remove = _userManager.RemoveFromRoleAsync(_user, currentRole).Result;
                if (remove.Succeeded)
                {
                    var x = _userManager.AddToRoleAsync(_user, newRole).Result;
                }
            }
            return Json(user);
        }
    }
}
