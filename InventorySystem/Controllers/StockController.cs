﻿using InventorySystem.Models;
using InventorySystem.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystem.Controllers
{
    [Authorize]
    public class StockController : Controller
    {
        private readonly AppdbContext _context;
        public StockController(AppdbContext context)
        {
            _context = context;
        }
        public IActionResult Index() => View();

        public JsonResult GetAll()
        {
            var q1 = _context.Purchases
            .GroupBy(r => r.ProductName)
            .Select(a => new
            {
                Name = a.Key,
                Qty = a.Sum(r => r.Quantity)
            }).ToList();

            var q2 = _context.Sales
           .GroupBy(r => r.ProductName)
           .Select(a => new
           {
               Name = a.Key,
               Qty = a.Sum(r => r.Quantity)
           }).ToList();


            var q3 = (from P in q1
                      join s in q2 on P.Name equals s.Name
                      group new { P, s } by P.Name
                      into v
                      select new
                      {
                          Name = v.Key,
                          Purchase = v.Sum(x => x.P.Qty),
                          Sales = v.Sum(x => x.s.Qty),
                          InStock = v.Sum(x => x.P.Qty) - v.Sum(x => x.s.Qty)
                      }).ToList();

            return Json(q3);
        }
    }
}
