using System;

namespace InventorySystem.Models
{
    public class MonthlySale
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Sales { get; set; }

    }
}
