﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace InventorySystem.Models
{
    [Table("Users")]
    public class User : IdentityUser<int>
    { 
        public string Password { get; set; }

        //public Role role { get; set; }

    }
}
