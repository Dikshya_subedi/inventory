﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InventorySystem.Models;
using InventorySystem.Models.ViewModel;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace InventorySystem.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly AppdbContext _context;
        private readonly SignInManager<User> _signInManager;

        public UserController(AppdbContext context, SignInManager<User> signInManager)
        {
            _context = context;
            _signInManager = signInManager;
        }


        [HttpGet]
        public IActionResult Index(string msg)
        {
            ViewData["LoginMsg"] = msg;
            return View();
        }

       public IActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                _context.Users.Add(user);
                 _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        public IActionResult Logout()
        {
            _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
        
    }
}