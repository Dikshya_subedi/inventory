﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystem.Models
{
    public class ProductPrice
    {
        [StringLength(100)]
        public string Product { get; set; }
        public int Price { get; set; }
    }
}
