﻿using System.Linq;
using InventorySystem.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InventorySystem.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RolesController : Controller
    {

        private readonly AppdbContext _context;
        public RolesController(AppdbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        public JsonResult GetAll() => Json(_context.Roles.ToList());

        [HttpGet]
        public IActionResult GetForm(int? id)
        {
            var item = _context.Roles.Find(id);
            return PartialView("_form", item);
        }

        [HttpGet]
        public JsonResult Delete(int id)
        {
            var roles = _context.Roles.Find(id);
            _context.Remove(roles);
            _context.SaveChanges();
            return Json(roles);

        }

        [HttpPost]
        public JsonResult SaveForm(Role role)
        {
            if (ModelState.IsValid)
            {
                role.NormalizedName = role.Name.ToUpper();

                if (role.Id == 0)
                    _context.Add(role);
                else
                    _context.Update(role);

                _context.SaveChanges();
            }
            return Json(role);
        }

    }
}