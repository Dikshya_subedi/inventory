﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventorySystem.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InventorySystem.Controllers
{
    [Authorize]
    public class CustomerController : Controller
    {

        private readonly AppdbContext _context;
        public CustomerController(AppdbContext context)
        {
            _context = context;
        }
        public IActionResult Index() => View();
        public JsonResult GetAll() => Json(_context.Customers.ToList());

        [HttpPost]
        public JsonResult SaveForm([Bind("Id,Name,PhoneNo,Date")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                if (customer.Id == 0)
                    _context.Add(customer);
                else
                    _context.Update(customer);

                _context.SaveChanges();
            }
            return Json(customer);
        }

        [HttpGet]
        public JsonResult Delete(int id)
        {
            var customer = _context.Customers.Find(id);
            _context.Remove(customer);
            _context.SaveChanges();
            return Json(customer);

        }


        [HttpGet]
        public IActionResult GetForm(int? id)
        {
            var customer = _context.Customers.Find(id);
            return PartialView("_form", customer);
        }

    }
}