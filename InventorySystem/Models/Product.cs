﻿using System;
using System.ComponentModel.DataAnnotations;


namespace InventorySystem.Models
{
    public class Product
    {
        public int Id { get; set; }

        public string ProductName { get; set; }
        public int Category { get; set; }

        [Required]
        [Range(1, 100000)]
        public int PurchasePrice { get; set; }

        [Required]
        public int SalePrice { get; set; }

        public DateTime PurchaseDate { get; set; }

        public ProductType ProductType { get; set; }


    }
}
