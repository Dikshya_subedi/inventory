﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace InventorySystem.Models
{
    public class Purchase
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string ProductName { get; set; }

        [Required]
        public int Category { get; set; }

        public string Unit { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public int PurchasePrice { get; set; }

        [Required]
        public int SalePrice { get; set; }

        [Required]
        public float TotalAmount { get; set; }

        [Required]
        public DateTime PurchaseDate { get; set; }


    }
}
    
