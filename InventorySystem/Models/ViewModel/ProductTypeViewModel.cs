﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystem.Models.ViewModel
{
    public class ProductTypeViewModel
    {
        public ProductType type { get; set;}
        public List<ProductType> types { get; set;}
    }
}
