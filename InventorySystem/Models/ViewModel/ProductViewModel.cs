﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystem.Models.ViewModel
{
    public class ProductViewModel
    {
        public Product product { get; set; }
        public List<Product> products { get; set; }
        public List<SelectListItem> productTypes { get; set; }
    }
}
