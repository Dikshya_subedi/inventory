using System;
using System.ComponentModel.DataAnnotations;

namespace InventorySystem.Models
{
    public class Profit
    {
        [Key]
        public int Id { get; set; }

        public int SaleId { get; set; }

        [Required]
        public string ProductName { get; set; }

        [Required]
        public int Category { get; set; }

        [Required]
        public int PurchasePrice { get; set; }

        [Required]
        public int SalePrice { get; set; }

        [Required]
        public int ProfitAmount { get; set; }

        public DateTime CreatedDate { get; set; }


    }
}
