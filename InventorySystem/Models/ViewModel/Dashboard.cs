﻿namespace InventorySystem.Models.ViewModel
{
    public class Dashboard
    {
        public double TotalSales { get; set; }
        public double Sales { get; set; }
        public double TotalOrder { get; set; }
        public double Order { get; set; }
        public double TotalProfit { get; set; }
        public double Profit { get; set; }

        public string TopProduct { get; set; }
        public string TodaysTopProduct { get; set; }
    }
}
