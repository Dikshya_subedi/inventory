﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using InventorySystem.Models;
using InventorySystem.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using InventorySystem.Common;

namespace InventorySystem.Controllers
{
    [Authorize(Roles = "Admin,Manager")]
    public class HomeController : Controller
    {
        private readonly AppdbContext _context;
        QueryHelper _queryHelper = new QueryHelper();
        public HomeController(AppdbContext context)
        {
            _context = context;
        }

        public List<Sales> TopSellingProduct(bool isToday, int topSelection = 100)
        {

            var today = DateTime.Today.AddDays(-1).AddHours(23.99);
            var qry = "";
            if (isToday)
            {
                qry = $@"select top {topSelection} ProductName, sum(quantity) as Quantity from(
                            select * from dbo.sales where SalesDate > '{today}')t
                                group by productName order by sum(quantity) desc";
            }
            else
            {
                qry = $@"select top {topSelection} ProductName, sum(quantity) Quantity from dbo.sales 
                            group by productName order by sum(quantity) desc";
            }

            var data = _queryHelper.ExecSql<Sales>(qry, _context);
            return data;
        }


        public IActionResult Index()
        {

            var today = DateTime.Today.AddDays(-1).AddHours(23.99);
            int TotalOrder = _context.Billings.Count();
            int Order = _context.Billings.Where(b => b.BillDate.Date > today).Count();

            int TotalSales = _context.Sales.Sum(s => s.TotalAmount);
            int Sales = _context.Sales.Where(b => b.SalesDate > today).Sum(s => s.TotalAmount);

            int TotalProfit = _context.Sales.Sum(s => s.Profit);
            int Profit = _context.Sales.Where(b => b.SalesDate > today).Sum(s => s.Profit);

            var TodayTopProduct = TopSellingProduct(true, 1);
            string TodaysTopProduct = TodayTopProduct.Count() > 0 ? TodayTopProduct[0].ProductName : "";
            string TopProduct = TopSellingProduct(false, 1)[0].ProductName;

            Dashboard db = new Dashboard();
            db.Profit = Profit;
            db.TotalProfit = TotalProfit;
            db.Order = Order;
            db.TotalOrder = TotalOrder;
            db.Sales = Sales;
            db.TotalSales = TotalSales;
            db.TopProduct = TopProduct;
            db.TodaysTopProduct = TodaysTopProduct;

            return View(db);
        }


        public JsonResult GetReport()
        {
            var Saledata = _context.Sales.GroupBy(s => s.SalesDate.Date).Select(x => new { totalAmount = x.Sum(x => x.TotalAmount), salesDate = x.Key.ToString("yyyy/MM/dd") }).ToList();
            var Profitdata = _context.Sales.GroupBy(s => s.SalesDate.Date).Select(x => new { totalAmount = x.Sum(x => x.Profit), salesDate = x.Key.ToString("yyyy/MM/dd") }).ToList();
            var data = new { Saledata = Saledata, Profitdata = Profitdata };
            return Json(data);

        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
