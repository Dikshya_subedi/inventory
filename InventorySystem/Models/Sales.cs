﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace InventorySystem.Models
{
    public class Sales
    {
        public int Id { get; set; }
        public int BillNo { get; set; }

        [Required]
        [StringLength(100)]
        public string ProductName { get; set; }

        [Required]
        public int Category { get; set; }
        public string Unit { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public int PurchasePrice { get; set; }

        [Required]
        public int SalePrice { get; set; }

        [Required]
        public int TotalAmount { get; set; }

        [Required]
        public int Profit { get; set; }

        [Required]
        public DateTime SalesDate { get; set; }

        public Billing Billing { get; set; }

        public ProductType ProductType { get; set; }

    }
}
